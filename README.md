# Test.Codes

Uma ferramenta de linux para testar códigos com base nos casos de teste disponíveis na plataforma run.codes. 
* [Download](https://gitlab.com/reisaraujo.miguel/test.codes/-/releases)

# Executando

A ferramenta cosiste de um programa simples escrito em shell script que pode ser executado de três formas:

## 1. Passando o caminho direto
Você pode executar o programa passando o caminho até ele.
Exemplo: ```~/Downloads/testcode```

## 2. Copiando manualmente para a pasta /bin
Você pode copiar o programa para a pasta `~/.local/bin/` para não precisar passar o caminho sempre que quiser usar.
Exemplo: ```cp ~/Downloads/testcode ~/.local/bin/```

Depois que fizer isso você deverá dar permissão para o seu usuário executar o programa:
```chmod +x ~/.local/bin/testcode```

Assim o script pode ser usado como um programa cli comum, apenas escrevendo "testcode" no terminal.

## 3. Usando o instalador
O instalador é um script simples que automatiza o processo descrito anteriormente.
Exemplo: ```./install.sh```

# Como usar
A forma de uso padrão é no formato ```testcode -f <arquivo>```, onde o "arquivo" é o seu código fonte. Você também pode passar a opção ```-d <diretório>``` para especificar o caminho até a pasta onde ficam os casos de teste, caso essa opção seja omitida será usado o valor padrão (./test).

O caminho para o código fonte também possui um valor padrão (./*.c) para usar o valor padrão basta passar o argumento ```-p```. Você pode ler as opções disponíveis no script usando o argumento ```-h``` ou ```--help```.
